# _*_ coding: utf-8 _*_
from pyspark import SparkContext
from itertools import tee, izip

__author__ = 'miguegallardo16@hotmail.com'

file = "airports.csv"

content = SparkContext("local", "Test_app")

contload = content.textFile(file).cache()

var = contload.filter(lambda s: 'ES' in s)

ap = contload.map(lambda lines: (lines.split(',')[2], lines.split(',')[8])).reduceByKey(lambda a, b: a + b).collect()

print (ap[1][1].count('ES'))

for item in ap:
    print (item[0] + '====> ' + str(item[1].count('ES')))
