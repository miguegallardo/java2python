from pyspark import SparkContext
from itertools import tee, izip

__author__ = 'miguegallardo16@hotmail.com'

textfile = "passwd"

content = SparkContext("local", "Test_app")

text = content.textFile(textfile).cache()

ap = text.map(lambda lines: (lines.split(':')[0], (lines.split(':')[2], lines.split(':')[3]))).reduceByKey(lambda a, b: a + b).collect()

for item in ap:
    print ('USERNAME: ' + item[0] + '  UID:' + item[1][0] + ' GID:' + item[1][1])